#!/usr/bin/env python
import cv2
import rospy
import numpy as np
import tf
from nav_msgs.msg import OccupancyGrid
from nav_msgs.msg import Odometry
from nav_msgs.srv import GetMap

class mapPublisher:
	def __init__(self):
		rospy.init_node("mapPublisher")

		self.map_link = tf.TransformBroadcaster()
		# filename_1= "/home/turk/catkin_ws/src/zeabus/zeabus_map/map/mymap.pgm"
		# #filename_2= "/home/turk/catkin_ws/src/zeabus/zeabus_map/map/transdec.pgm"
		self.resolution = 0.05
		self.origin_x = -100
		self.origin_y = -100
		self.origin_z = 0

		self.width = 4000
		self.height = 4000

		self.depthNow = 0
		self.depthOld = 0

		self.msg = self.loadMapinfo()

		rospy.Subscriber("/auv/state",Odometry,self.changeDepth)
		rospy.Service("static_map", GetMap , self.Response)
		self.pub = rospy.Publisher("map", OccupancyGrid, queue_size=5,latch=True)

	def loadMapinfo(self):
		msg = OccupancyGrid()

		msg.header.stamp = rospy.get_rostime()
		msg.header.frame_id = "map"

		msg.info.map_load_time = rospy.get_rostime()
		msg.info.resolution = self.resolution
		msg.info.width = self.width
		msg.info.height = self.height

		msg.info.origin.position.x = self.origin_x
		msg.info.origin.position.y = self.origin_y

		msg.info.origin.orientation.x = 0
		msg.info.origin.orientation.y = 0
		msg.info.origin.orientation.z = 0
		msg.info.origin.orientation.w = 1

		x = [0 for i in range(self.width * self.height)]
		msg.data = x

		return msg

	def Response(self,req):
		return self.msg
	def changeDepth(self,msg):
		self.depthNow = abs(int(round( msg.pose.pose.position.z / 0.5 ))) * 0.5
		#print self.depthNow
		if self.depthNow != self.depthOld:
			self.depthOld = self.depthNow
			print self.depthNow
			self.msg.info.origin.position.z = 0
			self.pub.publish(self.msg)
			#-self.depthNow
		self.map_link.sendTransform((0,0,self.depthNow),(0,0,0,1),rospy.Time.now(),"odom","map")
	def debug(self):
		rospy.loginfo("START PUBLISH MAP")
		self.pub.publish(self.msg)
		self.map_link.sendTransform((0,0,0),(0,0,0,1),rospy.Time.now(),"odom" ,"map")
		#self.broadcast(0)
		return
def callback(msg):
	print len(msg.data)
if __name__ == "__main__":

	x = mapPublisher()
	x.debug()
	rospy.spin()
	#rospy.init_node("mapPublisher")
	#rospy.Subscriber("/map",OccupancyGrid,callback)
	#rospy.spin()

